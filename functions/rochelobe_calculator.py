"""
Script containing a routine that finds the equipotentials of the roche lobe (at different potential values. i.e. for L1 L2 L3)

system is centered on CM (i.e. x = 0 is location of CM)
"""

import numpy as np

# from sympy.solvers import solve

# from sympy import Float
# from sympy import Symbol
# import sympy as sym

# from ballistic_integrator.functions.lagrange_points import *


# def L_point_solver(mu): #Finds the L2 and L3 points of the system
#     x = Symbol('x')
#     sol_L2 = solve(x-mu/((x-1+mu)**2) - (1-mu)/((x+mu)**2),x)
#     sol_L3 = solve(x-mu1/((x-1+mu1)**2) - (1-mu1)/((x+mu1)**2),x)
#     return  sol_L2[0], sol_L3[0]

# #Finds the element of the array that is nearest the desired value
# def find_nearest(array, value):
#     array = np.asarray(array)
#     idx = (np.abs(array - value)).argmin()
#     for count, element in enumerate(array):
#         if element == array[idx]:
#             index = count
#     return array[idx], index

# #Calculates x and y from polar co-ordinates, as well as the potential at each point
# def potential_solver(r, th, phis):
#     x=r*np.cos(th)
#     y=r*np.sin(th)
#     phi=(-mu/((x-1+mu)**2 +y**2)**0.5 - (1-mu)/((x+mu)**2 + y**2)**0.5 - 0.5*(x**2 +y**2))
#     np.savetxt(f'equi_test.dat', np.transpose([phi, x, y]))
#     for count1, i in enumerate(phi):
#         value = phis
#         phiv, index = find_nearest(phi,value)
#         xvals.append(x[index])
#         yvals.append(y[index])
#         phival.append(phiv)
#     return x, y


# # #Appends x and y value to a file, which can be imported into mass_loss_from_L2_master.py
# # def loop(q, L2, phis):
# #     for theta in th:
# #         lower = float(L2)-0.001
# #         upper = 2*float(L2)
# #         r=np.linspace(lower,upper,200)
# #         x, y = potential_solver(r,theta, phis)
# #     np.savetxt(f'equi{int(100*q)}.dat', np.transpose([phival, xvals, yvals]))

# #Function that takes values of the roche lobe for each star, from the UoC roche lobe calculator
# #and plots them to determine a polygon in which the ejected particle can't enter
# def plot_rochelobe(i):
#     rvals1, xvals1, rvals2, xvals2=[], [], [], []
#     #Reading in radius and theta values from the Roche data file of the corresponding q value
#     r1, r2 = np.loadtxt(f"roche_data/star1_{int(q*100)}Output.txt"), np.loadtxt(f"roche_data/star2_{int(q*100)}Output.txt")
#     th, th2 = np.linspace(-np.pi-np.pi/2,np.pi-np.pi/2,361), np.linspace(-np.pi+np.pi/2,np.pi+np.pi/2,361)

#     #Translating polar co-ordinates into Cartesian
#     for count, i in enumerate(th):
#         xr1 = r1[count]*a*np.cos(i) - mu
#         yr1 = r1[count]*a*np.sin(i)
#         rvals1.append([xr1, yr1])
#         xvals1.append(xr1)

#     for count, i in enumerate(th2):
#         xr2 = r2[count]*a*np.cos(i) + mu1
#         yr2 = r2[count]*a*np.sin(i)
#         rvals2.append([xr2, yr2])
#         xvals2.append(xr2)
#     rvals1, rvals2 =np.array(rvals1), np.array(rvals2)
#     poly1, poly2 = geo.Polygon(rvals1), geo.Polygon(rvals2)
#     x1, y1 = poly1.exterior.xy          #Defining the Roche lobe polygon
#     x2, y2 = poly2.exterior.xy
#     plt.plot(x1,y1, color='0.7', linewidth=0.8)
#     #plt.fill(x1,y1, color='0.7')           #Uncomment these lines to fill in the Roche lobe
#     #plt.fill(x2,y2, color='0.7')
#     plt.plot(x2,y2, color='0.7', linewidth=0.8)
#     return poly1, poly2

# def equipotential(q):
#     phitemp, x, y = np.loadtxt(f"equipotentials/equi{int(q*100)}.dat", unpack=True)
#     plt.plot(x, y ,linestyle = 'dashed', linewidth='0.8', color='0.5')


def return_phi(x, y, mu):
    """
    Function that returns the potential value (as described in Hubova+'19')

    quick reminder that everything is in terms of non-dimensionalized units.
    If you want real values then you should scale everything up by:
    TODO: this should be moved to the normal location that takes into account the frame of reference
    """

    phi = (
        -mu / ((x - 1 + mu) ** 2 + y**2) ** 0.5
        - (1 - mu) / ((x + mu) ** 2 + y**2) ** 0.5
        - 0.5 * (x**2 + y**2)
    )

    return phi


def plot_rochelobe_equipotentials_function(
    fig, ax, mass_donor, mass_accretor, separation
):
    """
    Function to plot the contour for the roche lobe
    """

    # System description
    q = mass_accretor / mass_donor

    M2 = 1
    M1 = M2 / q
    mu = M2 / (M1 + M2)
    mu1 = M1 / (M1 + M2)
    a = mu1 + mu

    # Get L1 location and the phi value at that point
    lagrange_points = calculate_lagrange_points(M1, M2, a)

    # Calculate potential values at those points
    phi_l1 = return_phi(lagrange_points["L1"][0], lagrange_points["L1"][1], mu)
    phi_l2 = return_phi(lagrange_points["L2"][0], lagrange_points["L2"][1], mu)
    phi_l3 = return_phi(lagrange_points["L3"][0], lagrange_points["L3"][1], mu)

    # # for ease, choose a lagrange point now
    # L_point = lagrange_points['L1']

    # #
    # theta_arr = np.linspace(0, 2*np.pi, 3600)
    # r_arr = np.linspace(L_point[0]-0.001, 2 * L_point[0], 200)

    # x_results = np.zeros(theta_arr.shape)
    # y_results = np.zeros(theta_arr.shape)
    # phi_results = np.zeros(theta_arr.shape)

    # for i, theta in enumerate(theta_arr):
    #     x_coords = r_arr * np.cos(theta)
    #     y_coords = r_arr * np.sin(theta)

    #     # Take the desired phi_value, and subtract that from the found phi list:
    #     phi_values = return_phi(x_coords, y_coords, mu)
    #     diff = phi_values-phi_l1
    #     idx = np.abs(diff).argmin()

    #     x_results[i] = x_coords[idx]
    #     y_results[i] = y_coords[idx]
    #     phi_results[i] = phi_values[idx]

    # Create a meshgrid for x and y
    xx, yy = np.meshgrid(np.linspace(-2, 2, 1000), np.linspace(-2, 2, 1000))

    # Calculate all the phi values
    phi_values = return_phi(xx, yy, mu)

    # Draw the contours
    CS = ax.contour(phi_values, levels=[phi_l1, phi_l2, phi_l3])

    fmt = {}
    strs = ["L1", "L2", "L3"]
    for l, s in zip(CS.levels, strs):
        fmt[l] = s

    # Label every other level using strings
    ax.clabel(CS, CS.levels, inline=True, fmt=fmt, fontsize=16)

    return fig, ax


if __name__ == "__main__":

    import matplotlib.pyplot as plt

    from david_phd_functions.plotting.utils import show_and_save_plot
    from david_phd_functions.plotting import custom_mpl_settings

    custom_mpl_settings.load_mpl_rc()

    fig, ax = plt.subplots()
    plot_rochelobe_contours(fig, ax, mass_donor=1, mass_accretor=0.5, separation=1)
    plt.show()
