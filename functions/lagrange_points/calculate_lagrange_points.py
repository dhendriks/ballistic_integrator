"""
Wrapper function that handles the prescription
"""

from ballistic_integrator.functions.lagrange_points.lagrange_points_binary_c import (
    calculate_lagrange_points_binary_c,
)


def calculate_lagrange_points(mass_accretor, mass_donor, separation, f=1):
    """
    Wrapper function that handles the prescription
    """

    # binary_c
    L_points = calculate_lagrange_points_binary_c(
        mass_donor, mass_accretor, separation=separation
    )

    return L_points
