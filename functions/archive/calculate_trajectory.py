"""
Script that contains the function to calculate the trajectory.

Based on the setup by Hubova 2019

TASKS:
- TODO: change setup to a class based method
- TODO: set up queue to handle more integrations in parallel
"""

import numpy as np

from scipy.integrate import ode

import astropy.units as u
import astropy.constants as const

from ballistic_integrator.functions.intersect import check_self_intersection
from ballistic_integrator.functions.integrator.integration_functions import (
    calc_angmom,
    calc_energy,
    dimensionless_roche_potential,
    calc_mu_accretor,
    RocheTrajectory,
)

from enum import Enum


class EXIT_CODES(Enum):
    """
    Enum class to containing simulaton status
    """

    NORMAL_FINISH = 0
    INTEGRATOR_TERMINATION = 1
    SELF_INTERSECTION = 2
    FALLBACK_INTO_ROCHELOBE = 3
    JACOBI_ERROR = 4
    WRONG_DIRECTION = 5
    CUSTOM_ERROR = 7


def calculate_trajectory(
    mass_donor,
    mass_accretor,
    separation,
    initial_position,
    initial_velocity,
    extra_settings=None,
    print_startup_info=True,
):
    """
    Function that integrates the ballistic orbit of a particle in a roche potential

    Returns a dict with the following entries:

    data_dict = {
        # System parameters
        'mass_accretor': mass_accretor,                             # Mass accretor in solarmasses
        'mass_donor': mass_donor,                                   # Mass donor in solarmasses
        'separation': separation,                                   # Separation in AU
        'mass_ratio_acc_don': mass_ratio_acc_don,                   # Mass ratio m_acc/m_don
        'orbital_angular_frequency': orbital_angular_frequency,     # Orbital angular frequency (rad/s)

        # Integration results
        'positions': positions,                                     # 2d array of positions ([x,y])
        'velocities': velocities,                                   # 2d array of velocities ([vx, vy])
        'energies': energies,                                       # Array of energies
        'angular_momenta': angular_momenta,                         # Array of angular momenta
        'jacobis': jacobis,                                         # Array of jacobi constant values
        'distances_from_center': distances_from_center,             # Array of distances from center

        # Integration status results
        'exit_code': EXIT_CODE,                                     # Exit code. See exit_code_dict
        'exit_code_message': EXIT_CODE_DICT[EXIT_CODE],             # Exit code message
    }
    """

    if not extra_settings:
        extra_settings = {}

    ###############################
    # Configuration

    # Controller parameters
    max_time = extra_settings.get("max_time", 100)
    dt = extra_settings.get("dt", 0.01)  # Play with this value a bit
    steps_check_self_intersection = extra_settings.get(
        "steps_check_self_intersection", 10
    )
    jacobi_error_tol = extra_settings.get("jacobi_error_tol", 1e-5)
    allow_fallback_to_center = extra_settings.get("allow_fallback_to_center", False)
    terminate_when_entering_rochelobes = extra_settings.get(
        "terminate_when_entering_rochelobes", False
    )
    print_info = extra_settings.get("print_info", True)

    # Handle custom stopping condition
    use_custom_termination_routine = extra_settings.get(
        "use_custom_termination_routine", False
    )
    custom_termination_routine = extra_settings.get("custom_termination_routine", None)
    custom_termination_ERROR_string = extra_settings.get(
        "custom_termination_error_string", ""
    )

    ###############################
    # Initialisation

    # Initial conditions
    t0 = 0
    step = 0
    initial_conditions = np.concatenate((initial_position, initial_velocity))
    EXIT_CODE = 0
    RUNNING = True

    self_intersection = False
    fallback = False
    wrong_direction = False

    # Calculate the initial things
    mass_ratio_acc_don = mass_accretor / mass_donor

    initial_energy = calc_energy(
        dimensionless_roche_potential(
            calc_mu_accretor(mass_donor=mass_donor, mass_accretor=mass_accretor),
            initial_position[0],
            initial_position[1],
        ),
        initial_position[0],
        initial_velocity[0],
        initial_position[1],
        initial_velocity[1],
    )
    initial_angular_momentum = calc_angmom(
        initial_position[0],
        initial_velocity[0],
        initial_position[1],
        initial_velocity[1],
    )
    initial_jacobi = initial_energy - initial_angular_momentum
    orbital_angular_frequency = np.sqrt(
        const.G
        * ((mass_accretor * u.solMass + mass_donor * u.solMass)).to(u.kg)
        / np.power((separation * u.au).to(u.m), 3)
    )
    orbital_period = 2 * np.pi / orbital_angular_frequency
    distance_from_center = np.sqrt(
        np.power(initial_position[0], 2) + np.power(initial_position[1], 2)
    )

    # Add initial values to lists
    positions = [initial_position]
    velocities = [initial_velocity]
    energies = [initial_energy]
    angular_momenta = [initial_angular_momentum]
    jacobis = [initial_jacobi]
    distances_from_center = [distance_from_center]
    times = [0]

    ########
    # Set up integrator:
    #   More info at https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.ode.html, https://stackoverflow.com/questions/12926393/using-adaptive-step-sizes-with-scipy-integrate-ode

    integrator = ode(RocheTrajectory)
    integrator.set_integrator("dopri5")
    integrator.set_initial_value(initial_conditions, t0)
    integrator.set_f_params(
        calc_mu_accretor(mass_donor=mass_donor, mass_accretor=mass_accretor)
    )

    ########
    # print start up info

    # Show some information
    if print_startup_info:
        # System info
        print("Mass Donor: {}".format(mass_donor))
        print("Mass accretor: {}".format(mass_accretor))
        print("Mass ratio (m_acc/m_don): {}".format(mass_ratio_acc_don))
        print("Separation: {}".format(separation))
        print("Angular frequency: {}".format(orbital_angular_frequency))
        print("Orbital period: {}".format(orbital_period.to(u.yr)))

        # Particle info
        print("initial position: {}".format(initial_position))
        print("initial velocity: {}".format(initial_velocity))
        print("initial angular momentum: {}".format(initial_angular_momentum))
        print("initial energy: {}".format(initial_energy))
        print("initial Jacobi const: {}".format(initial_jacobi))

    ###############################
    # Integration

    # Start of main loop
    while RUNNING:
        #############
        # Integrate to t + dt
        integrator.integrate(integrator.t + dt)

        #############
        # Select output and calculate the quantities that we need
        position = integrator.y[:2]
        velocity = integrator.y[2:]
        energy = calc_energy(
            dimensionless_roche_potential(
                calc_mu_accretor(mass_donor=mass_donor, mass_accretor=mass_accretor),
                position[0],
                position[1],
            ),
            position[0],
            velocity[0],
            position[1],
            velocity[1],
        )
        angular_momentum = calc_angmom(
            position[0], velocity[0], position[1], velocity[1]
        )
        jacobi = energy - angular_momentum
        distance_from_center = np.sqrt(
            np.power(position[0], 2) + np.power(position[1], 2)
        )
        jacobi_deviation = np.abs((jacobi - initial_jacobi) / initial_jacobi)

        #############
        # Store output
        positions.append(position)
        velocities.append(velocity)
        energies.append(energy)
        angular_momenta.append(angular_momentum)
        jacobis.append(jacobi)
        distances_from_center.append(distance_from_center)
        times.append(integrator.t + dt)

        #############
        # Print current state
        if print_info:
            print("Time: {:.2e} (step {})".format(integrator.t + dt, step))

        #############
        # Handle termination

        ##########
        # Check for self intersection
        if not steps_check_self_intersection == -1:
            if (not step == 0) and (step % steps_check_self_intersection) == 0:
                print("checking self intersection")
                self_intersection = check_self_intersection(
                    step, steps_check_self_intersection, positions
                )

        ##########
        # Check for direction of particle by looking at the d-distance (but only after some step)
        if not allow_fallback_to_center:
            if step > 10:
                if distances_from_center[-1] - distances_from_center[-2] < 0:
                    wrong_direction = True

        ##########
        # Fallback
        fallback = False
        if terminate_when_entering_rochelobes:
            pass

        # Check for termination:
        if not integrator.t < max_time:  # Check for exceeding max time
            RUNNING = False  # Terminate, but with no extra exit code

        elif not integrator.successful():  # Check for successful integration
            RUNNING = False
            EXIT_CODE = 1  # Flag the exit code

        elif self_intersection:  # Check if the trajectory is self interstecting
            RUNNING = False
            EXIT_CODE = 2  #

        elif fallback:  # Check for if the particle falls back into the roche lobes
            RUNNING = False
            EXIT_CODE = 3  #

        elif (
            jacobi_deviation > jacobi_error_tol
        ):  # Check if the jacobi constant is really constant
            RUNNING = False
            EXIT_CODE = 4  #

        elif wrong_direction:
            RUNNING = False
            EXIT_CODE = 5

        elif use_custom_termination_routine:
            """
            If the custom termination routine returns True then the simulation needs to be stopped
            """

            custom_terminatation = custom_termination_routine()

            if custom_terminatation:
                RUNNING = False
                EXIT_CODE = EXIT_CODES.CUSTOM_ERROR

        #
        step += 1

    # Error codes
    EXIT_CODE_DICT = {
        0: "Integration finished succesfully. Terminated by max_time condition",
        1: "Integrator failed with error code:\n{}".format(
            integrator.get_return_code()
        ),
        2: "Trajectory self-intersected",
        3: "Trajectory fallback",
        4: "Jacobi constant not constant",
        5: "Particle falling back to center",
    }
    if use_custom_termination_routine:
        EXIT_CODE_DICT[EXIT_CODES.CUSTOM_ERROR] = custom_termination_ERROR_string

    #############
    # Message at the end
    print(
        "Integration ended:\n"
        "\tTime: {}\n"
        "\tStep: {}\n"
        "\tTerminated: {}\n"
        "\texit code: {}\n"
        "".format(
            integrator.t,
            step,
            True if EXIT_CODE.value > 0 else False,
            EXIT_CODE_DICT[EXIT_CODE],
        )
    )

    # Change shape arrays
    positions = np.array(positions)
    velocities = np.array(velocities)

    data_dict = {
        # System parameters
        "mass_accretor": mass_accretor,
        "mass_donor": mass_donor,
        "separation": separation,
        "mass_ratio_acc_don": mass_ratio_acc_don,
        "orbital_angular_frequency": orbital_angular_frequency.value,
        "extra_settings": extra_settings,
        # Integration results
        "x_positions": positions[:, 0],
        "y_positions": positions[:, 1],
        "vx_velocities": velocities[:, 0],
        "vy_velocities": velocities[:, 1],
        # Times
        "times": times,
        # Other properties of the particle
        "energies": energies,
        "angular_momenta": angular_momenta,
        "jacobis": jacobis,
        "distances_from_center": distances_from_center,
        # Integration status results
        "exit_code": EXIT_CODE,
        "exit_code_message": EXIT_CODE_DICT[EXIT_CODE],
    }

    return data_dict
